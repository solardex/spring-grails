package com.lab.demo.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.lab.demo.domain.Car;

public interface CarRepository extends JpaRepository<Car, Long> {

    // @Query("SELECT c FROM car c WHERE u.name LIKE %?1%"
    //         + " OR c.vendor LIKE %?1%"
    // )
    // public List<Car> contains(String keyword);

}
